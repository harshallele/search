let modifiedSecurities = [];

let searchBtn = document.getElementById("btn-search");
let cardDiv = document.getElementById("cards");
let searchInput = document.getElementById("search-input");
let cardTemplate = ""

function getSecurities(){
    return axios.get("data");
}

function getCard(){
    return axios.get("card.mustache");
}

axios.all([getCard(),getSecurities()])
    .then(axios.spread(function(cardResp,secResp){

        cardTemplate = cardResp.data;
        let renderedCards = prepareCardData(secResp.data);

        
        cardDiv.innerHTML = renderedCards; 
    }))


searchBtn.addEventListener("click",(event) => {
    if(searchInput.value != ""){
        cardDiv.innerHTML = "";
        let searchText = searchInput.value;
        let renderedText = "";
        
        if(modifiedSecurities.length > 0){
            modifiedSecurities.forEach((sec) => {
                if(sec.valuesString.indexOf(searchText) >= 0){
                    renderedText += Mustache.render(cardTemplate,sec);
                }
            });

            cardDiv.innerHTML = renderedText;
        }
        else{
            axios.get("data?q=" + searchInput.value)
                .then(function(response){
                    renderedText = prepareCardData(response.data);

                    cardDiv.innerHTML = renderedText;
                })
        }
    }

});


function prepareCardData(secArr){
    
    let renderedCards = "";
    secArr.forEach(sec => {
        sec.yield = parseFloat(sec.offer_yield).toFixed(2);

        let bidPrice = parseFloat(sec.bid_price).toFixed(2);
        let offerPrice = parseFloat(sec.offer_price).toFixed(2);

        sec.bidOffer = bidPrice + "/" + offerPrice;
        
        let r1 = sec.rating1 == "NONE" ?  "-" : sec.rating1;
        let r2 = sec.rating2 == "NONE" ?  "-" : sec.rating2;
        let r3 = sec.rating3 == "NONE" ?  "-" : sec.rating3;

        sec.ratings = r1 + "/" + r2 + "/" + r3;
        
        sec.valuesString = Object.values(sec).toString();

        modifiedSecurities.push(sec);

        renderedCards += Mustache.render(cardTemplate,sec);

    });

    return renderedCards;
}